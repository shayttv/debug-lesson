#include "part1.h"
#include <iostream>

char* string_copy(char* dest, unsigned int destsize, const char* src)
{
    char* ret = dest;
    while (*src && destsize > 1) {
        *dest++ = *src++;
        destsize--;
    }
    *dest = '\0';
    return ret;
}

void part1()
{
    char password[] = "secret";
    char dest[12];
    const char src[] = "hello world!";

    string_copy(dest, sizeof(dest), src);

    std::cout << src << std::endl;
    std::cout << dest << std::endl;
}

